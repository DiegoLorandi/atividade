<?php
defined('BASEPATH') or exit('No direct exit access alowed');
include_once APPPATH.'libraries/util/CI_Object.php';
class ContaDataBuilder extends CI_Object{
    $contas = [
        [
            'parceiro'=>'Magalu',
            'descricao'=>'Notebook Asus VivoBook',
            'valor'=>'2000',
            'mes'=>06,
            'ano'=>2021,
            'tipo'=>'pagar'
        ],

        [
            'parceiro'=>'Casas Bahia',
            'descricao'=>'Notebook Asus VivoBook',
            'valor'=>'1800.25',
            'mes'=>06,
            'ano'=>2021,
            'tipo'=>'pagar'
        ],

        [
            'parceiro'=>'Aluguel',
            'descricao'=>'casa alugada',
            'valor'=>'4000',
            'mes'=>06,
            'ano'=>2021,
            'tipo'=>'receber'
        ],
    ];
    public function start(){
        $this->load->library('conta');
        foreach($this->contas as $conta){

        }
        $this->conta->cria($conta);
    }
    public function clear(){
        $this->db->truncate('conta');
    }
}
?>