<?php
class Table{

  private $data;
  private $label;

  function __construct(array $data, array $label){
    $this->data = $data;
    $this->label = $label;
  }

  private function header(){
    $this->html = '<thead> <tr>';
    foreach($this->label as $col){
      $this->html .= '<th scope="col">'.$col.'</th>';
    }
    $this->html .='</tr> </thead>';
    
    return $this->html;
  }

  private function body(){
    $this->html = '
      <tbody>'
        .$this->rows().
      '</tbody>';
    return $this->html;
  }

  private function rows(){
    $i = 0;
    foreach ($this->data as $row){
      $this->html ='<tr>';
        foreach ($row as $col){
          $this->html .='<td>'. $col .'</td>';
        }
      $this->html .='</tr>';
    }

    return $this->html;
  }

  public function getHTML(){
      $this->html = '
        <table class="table">'.
      $this->header().
      $this->body().
      
        '</table>';
      return $this->html;
   }

}
?>