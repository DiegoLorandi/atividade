<?php
defined('BASEPATH') or exit('No direct exit access alowed');
require APPPATH.'libraries/rest/MyRestController.php';
class CostasRest extends MyRestController{
    function __construct(){
        parent::__construct('contas');
    }
    
    function delete_conta_post(){
        $res = $this->model->delete_conta();
        $this->response($res,RESTController::HTTP_OK);
    }

    function status_conta_get(){
        $res = $this->model->status_conta();
        $this->response($res,RESTController::HTTP_OK);
    }
   
?>