<div style="height: 100vh">
  <div class="flex-center flex-column">
    <h3 class="mb-5">Cadastro Funcionários de Contabilidade</h3>

    <form method='POST'>
      <div class="form-outline mb-4">
        <input type="email" id="form2Example1" name="email" class="form-control" />
        <label class="form-label" for="form2Example1">Endereço de E-mail</label>
      </div>

      <div class="form-outline mb-4">
        <input type="password" id="form2Example2" name="senha" class="form-control" />
        <label class="form-label" for="form2Example2">Senha</label>
      </div>

      <button type="submit" class="btn btn-primary btn-block mb-4">Entrar</button>
      <p class="red-text"><?= $error ? 'Dados de Acesso Incorretos' : ''?></p>
    </form>

  </div>
</div>