<div class="row text-center"> 
  <div class="col-md-2">
    <a
      class="btn btn-primary mt-5"
      data-mdb-toggle="collapse"
      href="#collapseForm"
      role="button"
      aria-expanded="false"
      aria-controls="collapseForm"
      >
      Nova Conta
    </a>
  </div>
  <div class="col-md-2 offset-md-7 mt-3">
    <input type="month" id="month" name="month" value="<?= set_value('month')?>">
    </input>
  </div> 
</div>

<?php echo form_error('parceiro', '<div class="alert alert-danger">', '</div>'); ?>
<?php echo form_error('descricao', '<div class="alert alert-danger">', '</div>'); ?>
<?php echo form_error('valor', '<div class="alert alert-danger">', '</div>'); ?>
<?php echo form_error('mes', '<div class="alert alert-danger">', '</div>'); ?>
<?php echo form_error('ano', '<div class="alert alert-danger">', '</div>'); ?>

<div class="collapse " id="collapseForm">
    <div class="container pt-5">
        <div class="row">
            <div class="col-md-6 pt-5 mx-auto border">
                <form method="POST">
                    <input class="form-control" value="<?= set_value('parceiro') ?>"  type="text"              id="parceiro"  name="parceiro" placeholder="Devedor/Credor"><br/>
                    <input class="form-control" value="<?= set_value('descricao') ?>" type="text"              id="descricao" name="descricao" placeholder="Descrição"><br/>
                    <input class="form-control" value="<?= set_value('valor') ?>"     type="number" step=".01" id="valor"     name="valor" placeholder="Valor">
                    <input type="hidden" name="id" id="conta_id">
                    <input type="hidden" name="tipo" value="<?= $tipo ?>" placeholder="Tipo">
                    

                    <br/>
                    <br/>
                    <br/>

                    <button type="submit" class="btn btn-primary btn-block">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row mt-5">
    <div class="col">
        <?= $lista ?>
  </div>
</div>


<div
  class="modal fade"
  id="exampleModal"
  tabindex="-1"
  aria-labelledby="exampleModalLabel"
  aria-hidden="true"
>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Remoção de Conta</h5>
        <button
          type="button"
          class="btn-close"
          data-mdb-dismiss="modal"
          aria-label="Close"
        ></button>
      </div>
      <div class="modal-body">Realmente deseja deletar essa conta ? Essa ação é irreversível</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">Cancelar</button>
        <button type="button" id="confirmBtn" class="btn btn-primary">Confirmar</button>
      </div>
    </div>
  </div>
</div>

<script>
row_id = 0;
    $(document).ready(function(){
      $('#month').change(loadMonth);
      $('.delete_btn').click(openModal);
      $('#confirmBtn').click(deleteRow);
      $('.edit_btn').click(exibeForm);
      $('.pay_btn').click(liquidaConta);
    });

    function loadMonth(){
      var data = this.value.split('-');
      var ano = data[0];
      var mes = data[1];

      var v = window.location.href.split('/');
      var url = v.slice(0, 6).join('/');
      url = url +'/'+ mes +'/'+ ano;
      window.location.href = url;
    }

    function exibeForm(){
      row_id = this.id;

      var td = $('#'+row_id).parent().parent().parent().children();
      $('#parceiro'). val($(td[0]).text());
      $('#descricao').val($(td[1]).text());
      $('#valor').    val($(td[2]).text());
      $('#mes').      val($(td[3]).text());
      $('#ano').      val($(td[4]).text());
      $('#conta_id'). val(row_id);

      $('#collapseForm').collapse('show');
    }

    function openModal(){
        row_id = this.id;
        

        $('$exampleModal').modal();
    }

    function deleteRow(){
        var id = row_id;
        $.post(api('contas','delete_conta'),{id},function(d,s,x){
            $('#'+row_id).parent().parent().parent().remove();
            $('$exampleModal').modal('hide');
        });
    }
    function liquidaConta(){
      var id = this.id;
        $.post(api('contas','status_conta'),{id},function(d,s,x){
          $('#'+id).toggleClass('text-muted green-text',);
        });
    }

</script>