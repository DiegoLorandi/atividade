
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Controle Financeiro</a>


    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="<?= base_url('home') ?>">Início</a>
        </li>
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-mdb-toggle="dropdown"
            aria-expanded="false"
          >
            Cadastro
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="<?= base_url('Usuario/cadastro') ?>">Usuário</a></li>
            <li><a class="dropdown-item" href="#">Conta Bancária</a></li>
            <li><a class="dropdown-item" href="#">Parceiros</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-mdb-toggle="dropdown"
            aria-expanded="false"
          >
            Lançamentos
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="<?= base_url('contas/pagar') ?>">Contas a Pagar</a></li>
            <li><a class="dropdown-item" href="<?= base_url('contas/receber') ?>">Contas a Receber</a></li>
            <li><a class="dropdown-item" href="#">Fluxo de Caixa</a></li>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a
            class="nav-link dropdown-toggle"
            href="#"
            id="navbarDropdown"
            role="button"
            data-mdb-toggle="dropdown"
            aria-expanded="false"
          >
            Relatórios
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="#">Lançamentos por Período</a></li>
            <li><a class="dropdown-item" href="<?= base_url('contas/movimento') ?>">Movimento de Caixa</a></li>
            <li><a class="dropdown-item" href="#">Resumo Anual</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>