<?php

class First extends TestCase{
    public function setUp(): void {
        $this->resetInstance();
    }
    function testHello(){
        $this->CI->load->library('conta');
        $res = $this->CI->conta->soma(3,2);
        $this->assertEquals(5,$res);
    }
    function testContaDeveRetornarRegistrosDoMesDezembro() {
        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar',5,2021);
        $this->assertEquals(2,sizeof($res));
    }

    function testContaDeveInserirRegistroCorretamente(){
        //cenario
        $this->CI->load->library('Builder/ContaDataBuilder',null,'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        //ação
        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar',06,2021);

        //verificação
        $this->assertEquals(2,sizeof($res));
        $this->assertEquals('Magalu',$res[0]['parceiro']);
        $this->assertEquals(6,$res[0]['mes']);
        $this->assertEquals(2021,$res[0]['ano']);

        $this->assertEquals(2,sizeof($res));
        $this->assertEquals('Casas Bahia',$res[1]['parceiro']);
        $this->assertEquals(6,$res[1]['mes']);
        $this->assertEquals(2021,$res[1]['ano']);

        $this->assertEquals(1,sizeof($res));
        $this->assertEquals('Aluguel',$res[2]['parceiro']);
        $this->assertEquals(6,$res[2]['mes']);
    }
    function testContaDeveInformarTotalDeContasAPagarEAReceber(){
        //cenario
        $this->CI->load->library('Builder/ContaDataBuilder',null,'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        //ação
        $this->CI->load->library('conta');
        $res = $this->CI->conta->total('pagar',06,2021);
        //verificação
        $this->assertEquals(3800.25,$res);
    }
    function testContaDeveCalcularSaldoMensal(){
        //cenario
        $this->CI->load->library('Builder/ContaDataBuilder',null,'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        //ação
        $this->CI->load->library('conta');
        $res = $this->CI->conta->saldo(06,2021);

        //verificação
        $this->assertEquals(499,75,$res);
    }
}